//fetches the cucumber json from the locations in the config file and parses them to create pass/fail stats by job
console.log('================================================================================');
console.log('Start');
console.log('================================================================================');
var http = require('http');
var fs = require('fs');

var user = "chapmand";
var token = "7b94c59e8f4ea17721122a1f6b0de568";
var userAndToken = user + ':' + token;
var outputFilename = 'my.json';
var configObj = require("./config.json");
console.log('CONFIG:\n');
console.log(configObj);

var options = {
  hostname: configObj.hostname,
  port: 80,
  path: '/job/?',//set from list in config
  method: 'POST',
  auth: userAndToken
};

var allOfBody = {cukePaths: [], fetchCPidx: 0, fetchPidx: 0};
var error = { jobName: 'no error',
              lastConnectionAttempt: 'N/A'};

(function doMainStuff() {
  console.log('doMainStuff');
  writeOutStatus();

  //loop through cukePaths array,
  //for each loop through the paths array fetching the json and store no of scenarios passing/failing
  //(need to count number of scenarios in json - json is an array of features with elements array holding the scenarios,
  // then need to count the number of failing scenarios - any scenario with at least 1 failing step)
  //then loop through stored passing/failing stats and output as json in file,
  //one item in array per cuke-path (by job name)
  loadCukePathsIntoAllOfBody();

  if (allOfBody.cukePaths.length > 0) {
    //kick off the fetch loop, currently fetches one after another...
    fetchCucumber();
  } else {
    // try again later...
    console.log('try again in a minute');
    setTimeout(doMainStuff, 60 * 1000);//in a minute
  }

  function fetchCucumber(){
    //find a Jenkins job with cucumber paths degined
    while ( (allOfBody.fetchCPidx < allOfBody.cukePaths.length) &&
            ( (allOfBody.cukePaths[allOfBody.fetchCPidx].path.paths.length == 0) ||
              (allOfBody.fetchPidx >= allOfBody.cukePaths[allOfBody.fetchCPidx].path.paths.length) ) ){
      allOfBody.fetchCPidx++;
      allOfBody.fetchPidx = 0;
    }
    if (allOfBody.fetchCPidx >= allOfBody.cukePaths.length) {
      allOfBody.fetchCPidx = 0;
      allOfBody.fetchPidx = 0;
      //we're all done - now aggregate results within Jenkin job and then produce pass/fail stats by Jenkins job
      parseCucumberResults();
      //kick off the main loop in a while
      console.log('kick off again in 10 minutes');
      setTimeout(doMainStuff, 600 * 1000);//every 10 minutes
    } else {
      //fetch this item
      options.path = allOfBody.cukePaths[allOfBody.fetchCPidx].path.paths[allOfBody.fetchPidx];
      console.log('path:' + options.path);

      var req = http.request(options, function(res) {
        console.log('STATUS: ' + res.statusCode);
        //console.log('HEADERS: ' + JSON.stringify(res.headers));
        res.setEncoding('utf8');
        if (res.statusCode == 200) {
          res.on('data', function (chunk) {
            allOfBody.cukePaths[allOfBody.fetchCPidx].cukes[allOfBody.fetchPidx].cukeJson = allOfBody.cukePaths[allOfBody.fetchCPidx].cukes[allOfBody.fetchPidx].cukeJson + chunk;
          });
        } else {
          console.log('status not 200 on getting cuke json');
          res.on('data', function (chunk) {
            //do nothing
          });
        }

        res.on('end', function() {
          console.log('No more data in response:' + options.path);
          var d = new Date();
          console.log('mainLoop:' + d.toTimeString());
          //when all requests have returned then set timer for next iteration
          allOfBody.fetchPidx++;
          fetchCucumber();
        })
      });
      req.on('error', function(e) {
        console.log('problem with request: ' + e.message);
        error.connectionStatus = 'not connected';
        console.log('try again in an hour...');
        setTimeout(doMainStuff, 3600 * 1000);//try again in an hour
      });
      // write data to request body
      req.write(' ');
      req.end();
    }
  }
})();

function parseCucumberResults() {
  for (var cp = 0; cp < allOfBody.cukePaths.length; cp++) {
    for (var c = 0; c < allOfBody.cukePaths[cp].cukes.length; c++) {
      var noOfScenarios = 0;
      var noOfFailedScenarios = 0;
      var noOfMissingScenarios = 0;
      var f;
      var s;
      var st;
      try {
        var cukeJson = JSON.parse(allOfBody.cukePaths[cp].cukes[c].cukeJson);
        if (cukeJson.length) {
          for (f = 0; f < cukeJson.length; f++){
            if (cukeJson[f].elements) {
              for (s = 0; s < cukeJson[f].elements.length; s++) {
                noOfScenarios++;
                //is this a missing scenario?
                var scenarioIsMissing = false;
                if (cukeJson[f].elements[s].tags) {
                  for (var t = 0; t < cukeJson[f].elements[s].tags.length; t++) {
                    if (cukeJson[f].elements[s].tags[t].name == "@missing") {
                      scenarioIsMissing = true;
                    }
                  }
                }
                if (!scenarioIsMissing) {
                  //how many passing scenarios are there?  How many scenarios have at least all passing steps
                  var allPassingSteps;// = true;
                  if (cukeJson[f].elements[s].steps) {
                    //look for failed tests
                    for (st = 0; st < cukeJson[f].elements[s].steps.length; st++) {
                      if (cukeJson[f].elements[s].steps[st].result.status == "failed") {
                        allPassingSteps = false;
                        break;
                      } else { allPassingSteps = true; }
                    }
                    //Look for missing tests
                    for (st = 0; st < cukeJson[f].elements[s].steps.length; st++) {
                      if (cukeJson[f].elements[s].steps[st].result.status == "") {
                        scenarioIsMissing = true;
                        break;
                      }
                    }
                  } else console.log('no steps in this scenario, so skip:' + f + ':' + s + ':' + JSON.stringify(cukeJson[f].elements[s]).substring(0,10000) + '...:(no steps in this scenario)');
                  if (!allPassingSteps && !scenarioIsMissing) {
                    noOfFailedScenarios++;
                  } else if (scenarioIsMissing) {
                    noOfMissingScenarios++;
                  }
                } else {
                  noOfMissingScenarios++;
                }
              }
            } else console.log('no elements in this feature, so skip:' + f + ':' + JSON.stringify(cukeJson[f]).substring(0,10000) + '...:(no elements in this feature)');
          }
        } else console.log('no valid cuke json:' + JSON.stringify(cukeJson).substring(0,10000) + '...:(no valid cuke json)');
      } catch(e) {console.log('error reading json, so skip: ' + e + ':' + f + ':' + s + ':' + st + ':' +allOfBody.cukePaths[cp].cukes[c].cukeJson.substring(0,10000) + '...:(error reading json)');}
      allOfBody.cukePaths[cp].cukes[c].cukeJson = '';
      allOfBody.cukePaths[cp].cukes[c].noOfScenarios = noOfScenarios;
      allOfBody.cukePaths[cp].cukes[c].noOfFailedScenarios = noOfFailedScenarios;
      allOfBody.cukePaths[cp].cukes[c].noOfMissingScenarios = noOfMissingScenarios;
    }
  }
  //lastly we'll spin through the jenkins job paths (cukePaths) and sum up the total tests passing and failing
  for (var cp = 0; cp < allOfBody.cukePaths.length; cp++) {
    allOfBody.cukePaths[cp].total = 0;
    allOfBody.cukePaths[cp].totalFailing = 0;
    allOfBody.cukePaths[cp].totalMissing = 0;
    for (var c = 0; c < allOfBody.cukePaths[cp].cukes.length; c++) {
      allOfBody.cukePaths[cp].total = allOfBody.cukePaths[cp].total + allOfBody.cukePaths[cp].cukes[c].noOfScenarios;
      allOfBody.cukePaths[cp].totalFailing = allOfBody.cukePaths[cp].totalFailing + allOfBody.cukePaths[cp].cukes[c].noOfFailedScenarios;
      allOfBody.cukePaths[cp].totalMissing = allOfBody.cukePaths[cp].totalMissing + allOfBody.cukePaths[cp].cukes[c].noOfMissingScenarios;
    }
    allOfBody.cukePaths[cp].totalPassing = allOfBody.cukePaths[cp].total
                                           - allOfBody.cukePaths[cp].totalFailing
                                           - allOfBody.cukePaths[cp].totalMissing;
  }

  var d = new Date();
  allOfBody.lastUpdate = d.toLocaleString();
  fs.writeFile('cuke.json', JSON.stringify(allOfBody), function(err) {
    if(err) {
      console.log(err);
    } else {
      console.log("JSON saved to cuke.json");
    }
  });
}

function loadCukePathsIntoAllOfBody(){
  var cp;
  var p;
  for (cp = 0; cp <= configObj.cukePaths.length -1; cp++) {//put back var
    allOfBody.cukePaths[cp] = {path: configObj.cukePaths[cp], cukes: []};
    for (p = 0; p <= configObj.cukePaths[cp].paths.length -1; p++) {//put back var
      allOfBody.cukePaths[cp].cukes[p] = {path: configObj.cukePaths[cp].paths[p], cukeJson: ''};
    }
  }
  allOfBody.fetchCPidx = 0;
  allOfBody.fetchPidx = 0;
}

function writeOutStatus(){
  //write out the data to status.json
  var d = new Date();
  error.lastConnectionAttempt = d.toLocaleString();
  fs.writeFile('error.json', JSON.stringify(error), function(err) {
    if(err) {
      console.log(err);
    } else {
      console.log("JSON saved to error.json");
    }
  });
}
