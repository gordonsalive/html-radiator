console.log('================================================================================');
console.log('Start');
console.log('================================================================================');

var http = require('http');
var Qfs = require("q-io/fs");
var Q = require("q");
var _ = require("underscore");
Q.longStackSupport = true;

var DEBUG_CUKE_JSON_CHARS = 128;//10000 for debugging
var DEBUG = 0;

var user = "chapmand";
var token = "7b94c59e8f4ea17721122a1f6b0de568";
var userAndToken = user + ':' + token;
var configObj = require("./config.json");

var allOfBody = {
  jobs: [],
  cukes: {}
};
var error = { lastConnectionAttempt: 'N/A' };
writeOutStatus('failed');

(function doMainStuff() {
  var d = new Date();
  console.log('mainLoop:' + d.toLocaleString());

  loadCukePathsIntoAllOfBody();

  if (allOfBody.jobs.length > 0) {
    //kick off the fetch loop, currently fetches one after another...
    fetchCucumber();
  } else {
    // try again later...
    console.log('no jobs configured, try again in a minute');
    setTimeout(doMainStuff, 60 * 1000);//in a minute
  }

  function fetchCucumber() {
    if (DEBUG) console.log('>fetchCucumber');
    var cukes = [];//array of promised of cukes
    //spin through the unique list of jobs
    for (var j = 0; j < allOfBody.jobs.length; j++) {
      //find the paths for this job and fetch their cuke json
      var job = allOfBody.jobs[j];
      var paths = allOfBody.cukes[job].paths;
      //console.log(paths);
      for (var p = 0; p < paths.length; p++) {
        cukes.push(fetchCukeForPath(paths[p], job, p).then(addReplaceCuke));//add a promise of a cuke to the array
      }
    }
    var allCukes = Q.all(cukes);

    //aggregate results with Jenkins job and produce pass/fail stats by Jenkins job
    var finalResult = allCukes.then(parseCucumberResults)
      .then(writeOutResults);

    //handle failed promise or repeat in 5 minutes
    finalResult.then(function() {repeatInABit()}).fail(handleFail);
  }

  function repeatInABit(){
    var d = new Date();
    console.log('Completed: ' + d.toLocaleString() + '. Wait 5 minutes before repeating...')
    setTimeout(doMainStuff, 300 * 1000);/*try again in 5 minutes*/
  }

  function handleFail(err) {
    console.error(err);
    console.log('error, try again in 10 minutes');
    //try again in a whlie, leave an interval for things to correct themselves.
    setTimeout(doMainStuff, 600 * 1000);//try again in 10 minutes
  }

  function writeOutResults() {
    if (DEBUG) console.log('>writeOutResults');
    var d = new Date();
    allOfBody.lastUpdate = d.toLocaleString();
    return Qfs.write("cuke.json", JSON.stringify(allOfBody))
      .then(function(){writeOutStatus('succeeded')});
  }

  function parseCucumberResults() {
    if (DEBUG) console.log('>parseCucumberResults');
    return Q.Promise(function(resolve, reject, notify) {
      try {
        //spin through the unique list of jobs
        for (var j = 0; j < allOfBody.jobs.length; j++) {
          //find the paths for this job and fetch their cuke json
          var job = allOfBody.jobs[j];
          var paths = allOfBody.cukes[job].cukes;
          for (var p = 0; p < paths.length; p++) {
            updateTotalsForCuke(job, p);
          }
        }

        //lastly we'll spin through the jenkins job paths (cukePaths) and sum up the total tests passing and failing
        for (var j = 0; j < allOfBody.jobs.length; j++) {
          var job = allOfBody.jobs[j];
          var paths = allOfBody.cukes[job].cukes;

          allOfBody.cukes[job].total = 0;
          allOfBody.cukes[job].totalFailing = 0;
          allOfBody.cukes[job].totalMissing = 0;

          for (var p = 0; p < paths.length; p++) {
            allOfBody.cukes[job].total = allOfBody.cukes[job].total + allOfBody.cukes[job].cukes[p].noOfScenarios;
            allOfBody.cukes[job].totalFailing = allOfBody.cukes[job].totalFailing + allOfBody.cukes[job].cukes[p].noOfFailedScenarios;
            allOfBody.cukes[job].totalMissing = allOfBody.cukes[job].totalMissing + allOfBody.cukes[job].cukes[p].noOfMissingScenarios;
          }
          allOfBody.cukes[job].totalPassing = allOfBody.cukes[job].total
                                                 - allOfBody.cukes[job].totalFailing
                                                 - allOfBody.cukes[job].totalMissing;
        }

        resolve();
      }
      catch(err) {
        reject(err);
      }
    });
  }

  function updateTotalsForCuke(job, idx) {
    if (DEBUG) console.log('>updateTotalsForCuke(' + job + ', ' + idx + ')');
    var noOfScenarios = 0;
    var noOfFailedScenarios = 0;
    var noOfMissingScenarios = 0;
    try {
      var cukeJson = JSON.parse(allOfBody.cukes[job].cukes[idx].cukeJson);
      if (cukeJson.length) {
        for (var f = 0; f < cukeJson.length; f++){
          if (cukeJson[f].elements) {
            for (var s = 0; s < cukeJson[f].elements.length; s++) {
              noOfScenarios++;
              //is this a missing scenario?
              var scenarioIsMissing = false;
              if (cukeJson[f].elements[s].tags) {
                for (var t = 0; t < cukeJson[f].elements[s].tags.length; t++) {
                  if (cukeJson[f].elements[s].tags[t].name == "@missing") {
                    scenarioIsMissing = true;
                  }
                }
              }
              if (!scenarioIsMissing) {
                //how many passing scenarios are there?  How many scenarios have at least all passing steps
                var allPassingSteps;// = true;
                if (cukeJson[f].elements[s].steps) {
                  //look for failed tests
                  for (var st = 0; st < cukeJson[f].elements[s].steps.length; st++) {
                    if (cukeJson[f].elements[s].steps[st].result.status == "failed") {
                      allPassingSteps = false;
                      break;
                    } else { allPassingSteps = true; }
                  }
                  //Look for missing tests
                  for (var st = 0; st < cukeJson[f].elements[s].steps.length; st++) {
                    if (cukeJson[f].elements[s].steps[st].result.status == "") {
                      scenarioIsMissing = true;
                      break;
                    }
                  }
                } else if (DEBUG) console.log('no steps in this scenario, so skip:' + f + ':' + s + ':' + JSON.stringify(cukeJson[f].elements[s]).substring(0,DEBUG_CUKE_JSON_CHARS) + '...:(no steps in this scenario)');
                if (!allPassingSteps && !scenarioIsMissing) {
                  noOfFailedScenarios++;
                } else if (scenarioIsMissing) {
                  noOfMissingScenarios++;
                }
              } else {
                noOfMissingScenarios++;
              }
            }
          } else if (DEBUG) console.log('no elements in this feature, so skip:' + f + ':' + JSON.stringify(cukeJson[f]).substring(0,DEBUG_CUKE_JSON_CHARS) + '...:(no elements in this feature)');
        }
      } else if (DEBUG) console.log('no valid cuke json:' + JSON.stringify(cukeJson).substring(0,DEBUG_CUKE_JSON_CHARS) + '...:(no valid cuke json)');
    } catch(e) {if (DEBUG) console.log('error reading json, so skip: ' + e + ':' + job + ':' + idx + ':' + f + ':' + s + ':' + st + ':' /*+allOfBody.cukes[job].cukes[idx].cukeJson.substring(0,10000)*/ + '...:(error reading json)');}
    allOfBody.cukes[job].cukes[idx].cukeJson = '';
    allOfBody.cukes[job].cukes[idx].noOfScenarios = noOfScenarios;
    allOfBody.cukes[job].cukes[idx].noOfFailedScenarios = noOfFailedScenarios;
    allOfBody.cukes[job].cukes[idx].noOfMissingScenarios = noOfMissingScenarios;
  }

  function fetchCukeForPath(path, job, idx) {
    if (DEBUG) console.log('>fetchCukeForPath (' /*+ path + ', '*/ + job + ', ' + idx + ')');
    return Q.Promise(function(resolve, reject, notify) {
      var body = '';

      var options = {
        hostname: configObj.hostname,
        port: 80,
        path: path,
        method: 'POST',
        auth: userAndToken
      };

      //fetch this item
      var req = http.request(options, function(res) {
        if (DEBUG) console.log('STATUS: ' + res.statusCode + ' (' /*+ path + ', '*/ + job + ', ' + idx + ')');
        if (res.statusCode != 200) {
          writeOutStatus('failed');
          //some requests may end in error (404) if files are missing, need to tolerate this
          //reject(new Error("Status code was " + res.statusCode));
        }
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
          body = body + chunk;
        });

        res.on('end', function() {
          resolve({
            job: job,
            path: path,
            idx: idx,
            body: body
          });
        });
      });
      req.on('error', function(e) {
        reject(e);
      });
      // write data to request body
      req.write(' ');
      req.end();
    });
  }

  function addReplaceCuke(newCuke) {
    if (DEBUG) console.log('>addReplaceView (' + newCuke.job + ', ' + newCuke.idx + ')');
    return Q.Promise(function(resolve, reject, notify) {
      try {
        allOfBody.cukes[newCuke.job].cukes[newCuke.idx].cukeJson = newCuke.body;

        resolve();
      }
      catch(err) {
        reject(err);
      }
    });
  }

})();

function loadCukePathsIntoAllOfBody(){
  if (DEBUG) console.log('>loadCukePathsIntoAllOfBody');
  //combine the cukePaths arrays of all views into a distinct list
  //(1) create unique list of jobs from the lists in each of the views
  var jobs = configObj.views.map(
    function(view){return view.cukePaths.map(
      function(CPs){return CPs.job})});//gives me an array of arrays of job names
  allOfBody.jobs = _.union.apply(null, jobs);
  allOfBody.cukes = {};
  //(2) create skeleton holding cuke paths and place for cuke json for each path, for each job
  for (var v = 0; v < configObj.views.length; v++) {
    for (var cp = 0; cp <= configObj.views[v].cukePaths.length -1; cp++) {
      allOfBody.cukes[configObj.views[v].cukePaths[cp].job] = {paths: configObj.views[v].cukePaths[cp].paths, cukes: []};
      for (var p = 0; p <= configObj.views[v].cukePaths[cp].paths.length -1; p++) {
        allOfBody.cukes[configObj.views[v].cukePaths[cp].job].cukes[p]  = {path: configObj.views[v].cukePaths[cp].paths[p], cukeJson: ''};
      }
    }
  }
  //console.log(allOfBody);
}

function writeOutStatus(connectionStatus){
  if (DEBUG) console.log('>writeOutStatus(' + connectionStatus + ')');
  var d = new Date();
  var localeDate = d.toLocaleString();
  if (connectionStatus = 'succeeded') {
    error.connectionStatus = 'connected';
  } else {
    error.connectionStatus = 'not connected';
  }
  error.lastConnectionAttempt = localeDate;

  Qfs.write("error.json", JSON.stringify(error))
    .fail(console.error);
}
