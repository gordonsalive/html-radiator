var http = require('http')
var url = require('url')
var fs = require('fs')

console.log('================================================================================');
console.log('Start');
console.log('================================================================================');

//var temperature = 'cool'; //or 'hot', or 'meltdown'
var meltdownStatus = require("./meltdown.json");
console.log(meltdownStatus);
var temperature = meltdownStatus.status;

http.createServer(function (request, response) {
  var requestUrl = url.parse(request.url);
  var action = requestUrl.pathname;
  console.log(action);
  if (action == '/meltdown.html') {
    response.writeHead(200, {"Content-Type": "text/html"});
    //fetchData(function(){
    //  try {writeMeltdownPage(response)
    //  } finally {response.end()}}
    //);
    writeMeltdownPage(response)
    response.end();
  } else if ( (action == '/melt-circle.png') ||
              (action == '/hot-circle.png') ||
              (action == '/cool-circle.png') ||
              (action == '/melt-bar.png') ||
              (action == '/empty-bar.png') ||
              (action == '/hot-bar.png') ||
              (action == '/cool-bar.png') ||
              (action == '/melt-face.png') ||
              (action == '/hot-face.png') ||
              (action == '/cool-face.png') ||
              (action == '/background-2.png') ) {
     var img = fs.readFileSync('.' + action);
     response.writeHead(200, {'Content-Type': 'image/png' });
     response.end(img, 'binary');
   } else if ( (action == '/Thermometer.jpg') ||
               (action == '/melt.jpg') ||
               (action == '/hot.jpg') ||
               (action == '/cool.jpg') ) {
      var img = fs.readFileSync('.' + action);
      response.writeHead(200, {'Content-Type': 'image/jpg' });
      response.end(img, 'binary');
   } else {
    console.log('--FAILED:' + action);
    response.writeHead(500);
    response.end()
  }
}).listen(8082)

function writeMeltdownPage(response){
  response.write(writeHeader());

  function writeHeader(){
    //TODO add styles to tidy up the HTML
    var result = '<!DOCTYPE html><html><head><meta charset="UTF-8"><title>Automated Test Results</title><style></style></head>\n';
    result += writeBody();
    result += '</html>';
    return result;
  }

  function writeBody(){
    var result = '<body align="center" bgcolor="#181818" style="background-image: url(\'background-2.png\')">\n';
    result += writeTable();
    result += '</body>\n';
    return result;
  }

  function writeTable(){
    var result = '<table align="center" style="color:#EAEAEA; text-align:center; font-family:\'Segoe UI\'">\n';

    if (temperature == 'meltdown') {
      result += '<tr style="font-size:72px"><td>Darren Meltdown Status - BREAKDOWN!<td><tr>';
      result += '<tr><td width="640" height="960" background="melt.jpg" style="background-repeat: no-repeat">'
    } else if (temperature == 'hot') {
      result += '<tr style="font-size:72px"><td>Darren Meltdown Status -  HOT!<td><tr>';
      result += '<tr><td width="640" height="960" background="hot.jpg" style="background-repeat: no-repeat">'
    } else {
      result += '<tr style="font-size:72px"><td>Darren Meltdown Status - cool<td><tr>';
      result += '<tr><td width="640" height="960" background="cool.jpg" style="background-repeat: no-repeat">'
    }

    // //table had 4 rows, depending upong heat
    // //top row only ever red
    // result += '<tr><td>';//' width="50" height="400" background="melt-bar.png">';
    // if (temperature == 'meltdown') {
    //   result += '<img src="melt-bar.png" style="width:40px;height:400px;">';
    // } else {
    //   result += '<img src="empty-bar.png" style="width:40px;height:400px;">';
    // }
    // result += '</td></tr>\n';
    // //middle row, red or orange
    // result += '<tr><td>';
    // if (temperature == 'meltdown') {
    //   //result += '<td width="50" height="400" background="melt-bar.png">';
    //   result += '<img src="melt-bar.png" style="width:40px;height:400px;">';
    // } else if (temperature == 'hot') {
    //   //result += '<td width="50" height="400" background="hot-bar.png">';
    //   result += '<img src="hot-bar.png" style="width:40px;height:400px;">';
    // } else {
    //   result += '<img src="empty-bar.png" style="width:40px;height:400px;">';
    // }
    // result += '</td></tr>\n';
    // //bottom row, red, orange or green
    // result += '<tr><td>';
    // if (temperature == 'meltdown') {
    //   //result += '<td width="50" height="400" background="melt-bar.png">';
    //   result += '<img src="melt-bar.png" style="width:40px;height:400px;">';
    // } else if (temperature == 'hot') {
    //   //result += '<td width="50" height="400" background="hot-bar.png">';
    //   result += '<img src="hot-bar.png" style="width:40px;height:400px;">';
    // } else {
    //   //result += '<td width="50" height="400" background="cool-bar.png">';
    //   result += '<img src="cool-bar.png" style="width:40px;height:400px;">';
    // }
    // result += '</td></tr>\n';
    // //thermometer bulb at the bottom is red, orange or green with face in the middle
    // result += '<tr><td>';
    // if (temperature == 'meltdown') {
    //   //result += '<td width="400" height="400" background="melt-circle.png">';
    //   result += '<img src="melt-circle.png" style="width:409px;height:400px;">';
    // } else if (temperature == 'hot') {
    //   //result += '<td width="400" height="400" background="hot-circle.png">';
    //   result += '<img src="hot-circle.png" style="width:409px;height:400px;">';
    // } else {
    //   //result += '<td width="400" height="400" background="cool-circle.png">';
    //   result += '<img src="cool-circle.png" style="width:409px;height:400px;">';
    // }
    //
    // result += '</td></tr>\n';

    result += '</table>\n';
    return result;
  }





}
