console.log('================================================================================');
console.log('Start');
console.log('================================================================================');

var http = require('http');
var Qfs = require("q-io/fs");
var Q = require("q");
var _ = require("underscore");
Q.longStackSupport = true;

var user = "chapmand";
var token = "7b94c59e8f4ea17721122a1f6b0de568";
var userAndToken = user + ':' + token;
var configObj = require("./config.json");
var resultsJson = {
  passed: {
    someViewName: [{"name":"Some Job Name","color":"blue","lastSuccessfulBuild":{"id":"2015-12-18_14-10-05"},"lastUnsuccessfulBuild":null}]
  },
  failed: {}
};
var status = { connectionStatus: 'not connected',
               lastSuccessfullConnect: 'N/A',
               lastConnectionAttempt: 'N/A'};
writeOutStatus('failed');

(function doMainStuff() {
  var d = new Date();
  console.log('mainLoop:' + d.toTimeString());
  //read in the pass fail files if they are there
  var existingResults = parseExistingResults().fail(console.error);
  //request view json and replace it in memory (for each view in config)
  var finalResult = existingResults
    .then(requestViewsAndReplaceJson)
    .then(writeOutFiles);
  //handle failed promise or repease in 30 seconds
  finalResult
    .then(function() {setTimeout(doMainStuff, 30 * 1000);/*try again in 30 seconds*/}).fail(handleFail);

  function handleFail(err) {
    console.error(err);
    console.log('error, try again in 5 minutes');
    //try again in a whlie, leave an interval for things to correct themselves.
    setTimeout(doMainStuff, 300 * 1000);//try again in 5 minutes
  }

  function parseExistingResults() {
    return Q.all([
      Qfs.read("passed.json")
        .then(function(text) {return parseResults('passed', text)}),
      Qfs.read("failed.json")
        .then(function(text) {return parseResults('failed', text)})
    ])
  }

  function parseResults(type, text) {
    console.log('>parseResults ' + type + ' ----------------');
    return Q.Promise(function(resolve, reject, notify) {
      try {
        resultsJson[type] = JSON.parse(text)
        resolve();
      } catch(err) { reject(err) }
    });
  }

  function writeOutFiles() {
    //console.log('>writeOutFiles');
    return Q.all([
      Qfs.write("passed.json", JSON.stringify(resultsJson.passed)),
      Qfs.write("failed.json", JSON.stringify(resultsJson.failed))
    ]).then(function(){return writeOutStatus('succeeded')});
  }

  function requestViewsAndReplaceJson() {
    return Q.all(_.range(configObj.views.length).map(function(idx) {return requestViewJson(idx).then(addReplaceView);}));//this is now an array of promises
  }

  function requestViewJson(viewIndex) {
    //console.log('>requestViewJson (' + viewIndex + ')');
    return Q.Promise(function(resolve, reject, notify) {
      var allOfBody = '';

      var options = {
        hostname: configObj.hostname,
        port: 80,
        path: configObj.views[viewIndex].path,
        method: 'POST',
        auth: userAndToken
      };

      var req = http.request(options, function(res) {
        //console.log('STATUS: ' + res.statusCode);
        if (res.statusCode != 200) {
          writeOutStatus('failed');
          reject(new Error("Status code was " + res.statusCode));
        }
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
          allOfBody = allOfBody + chunk;
        });
        res.on('end', function() {
          resolve({
            view: configObj.views[viewIndex].name,
            body: allOfBody
          });
        })
      });
      req.on('error', function(e) {
        reject(e);
      });
      // write data to request body
      req.write(' ');
      req.end();
    });
  }

  function addReplaceView(viewWithBody){
    //console.log('>addReplaceView ' + viewWithBody.view);
    return Q.Promise(function(resolve, reject, notify) {
      try {
        var allOfBodyJson = JSON.parse(viewWithBody.body);

        resultsJson.passed[viewWithBody.view] = allOfBodyJson.jobs.filter(function (job) {
          return (job.color.lastIndexOf('blue') === 0);//starts with blue
        });

        resultsJson.failed[viewWithBody.view] = allOfBodyJson.jobs.filter(function (job) {
          return (job.color.lastIndexOf('blue') != 0);//starts with read or anything else not blue
        });

        resolve();
      }
      catch(err) {
        reject(err);
      }
    });
  }
})();

function writeOutStatus(connectionStatus){
  var d = new Date();
  var localeDate = d.toLocaleString();
  if (connectionStatus = 'succeeded') {
    status.lastSuccessfullConnect = localeDate;
    status.connectionStatus = 'connected';
  } else {
    status.connectionStatus = 'not connected';
  }
  //write out the data to status.json
  status.lastConnectionAttempt = localeDate;

  return Qfs.write("status.json", JSON.stringify(status))
    .fail(console.error);
}
