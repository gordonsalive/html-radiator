var http = require('http')
var url = require('url')
var fs = require('fs')

//TODO: are there memory leaks in this java code?  read up on the subject.

console.log('================================================================================');
console.log('Start');
console.log('================================================================================');

http.createServer(function (request, response) {
  var requestUrl = url.parse(request.url);
  var action = requestUrl.pathname;
  console.log(action);
  if (action == '/radiator.html') {
    response.writeHead(200, {"Content-Type": "text/html"});
    fetchData(function(){
      try {writeTestJobsPage(response, 'landscape')
      } finally {response.end()}}
    );
  } else if (action == '/radiator-vertical.html') {
    response.writeHead(200, {"Content-Type": "text/html"});
    fetchData(function(){
      try {writeTestJobsPage(response, 'portrait')
      } finally {response.end()}}
    );
  } else if ( (action == '/red-circle.png') ||
              (action == '/green-circle.png') ||
              (action == '/background-2.png') ) {
     var img = fs.readFileSync('.' + action);
     response.writeHead(200, {'Content-Type': 'image/png' });
     response.end(img, 'binary');
   } else if ( (action == '/red-circle-anime.gif') ||
               (action == '/green-circle-anime.gif') ) {
      var img = fs.readFileSync('.' + action);
      response.writeHead(200, {'Content-Type': 'image/gif' });
      response.end(img, 'binary');
  } else {
    console.log('--FAILED:' + action);
    response.writeHead(500);
    response.end()
  }
}).listen(8080)//9615

var jobsData = {
  name: 'Automated Test Results',
  lastSuccessfulFetch: '',
  lastAttemptedFetch: '',
  connectionStatus: 'not connected',
  jobsData: [{type: 'passed', completed: false, totalsCompleted: false, data: []},
             {type: 'failed', completed: false, totalsCompleted: false, data: []}]
}
function resetJobsData(){
  var x;
  for (x = 0; x < jobsData.jobsData.length; x++) {
    jobsData.jobsData[x].completed = false;
    jobsData.jobsData[x].totalsCompleted = false;
  }
}
function jobViewDataAllCompleted(){
  var result = (jobsData.jobsData.filter(function (job) {
    return ((job.completed == true));
  }).length == jobsData.jobsData.length);
  //console.log('all completed = ' + result);
  return result;
}
function allCompleted(){
  var result = (jobsData.jobsData.filter(function (job) {
    return ((job.completed == true) && (job.totalsCompleted == true));
  }).length == jobsData.jobsData.length);
  //console.log('all completed = ' + result);
  return result;
}
function indexOfJobType(type) {
  return jobsData.jobsData.map(function(job) {return job.type;}).indexOf(type);
}

//fetchQueue is an array of fetches to be carried out in sequence
//and associated checks to comfirm they are completed before kicking off next fetch
//(to run fetches in parallel, simply supply true in the check funcion)
var fetchQueue = [{fetchMethod: function(){}, completedCheck: function(){}, stepName: ''}];
function processFetchQueue(){
  function processFetch(remainingFetchQueue){
    if (remainingFetchQueue.length > 0) {
      //kick off the fetch
      remainingFetchQueue[0].fetchMethod();
      //process checks until done, it will then automatically kick off the next one
      processCheck(remainingFetchQueue);
    }
  }

  function processCheck(remainingFetchQueue){
    if (remainingFetchQueue.length > 0) {
      //perform the check, if false try it again in 10 seconds
      if (remainingFetchQueue[0].completedCheck()){
        //kick off the next fetch in the queue
        var newRemainingFetchQueue = [];
        for (var x = 1; x < remainingFetchQueue.length; x++) {
          newRemainingFetchQueue.push(remainingFetchQueue[x])
        }
        //processFetch(remainingFetchQueue.shift())
        processFetch(newRemainingFetchQueue);
      } else {
        console.log('try again in a sec!')
        //set this method off again in 10 seconds
        setTimeout(function(){processCheck(remainingFetchQueue)}, 10);
      }
    }
  }
  processFetch(fetchQueue);
}

function fetchData(doWritePage){
  console.log('fetch data');
  var d = new Date();
  resetJobsData();

  getStatus();
  // var index;
  // dataArray = jobsData.jobsData;
  // for	(index = 0; index < dataArray.length; index++) {
  //   getJobsData(dataArray[index].type, doWritePage);
  // };
  //set up the fetch methods
  fetchQueue = [];//clear out fetch queue
  var index;
  dataArray = jobsData.jobsData;
  fetchQueue.push({
    fetchMethod: function(){getJobsData('passed');},
    completedCheck: function(){return true;/*kick off these fetches in parallel*/},
    stepName: "passed Job data"
  });
  fetchQueue.push({
    fetchMethod: function(){getJobsData('failed');},
    completedCheck: function(){return jobViewDataAllCompleted();/*don't kick off anymore fetches until these are completed*/},
    stepName: "failed Job data"
  });
  //Next fetch is the totals data
  fetchQueue.push({
    fetchMethod: function(){fetchTotalsData('passed');},
    completedCheck: function(){return true;/*run these tasks in parallel*/},
    stepName: "passed Totals data"
  });
  fetchQueue.push({
    fetchMethod: function(){fetchTotalsData('failed');},
    completedCheck: function(){return allCompleted();},
    stepName: "failed Totals data"
  });
  //finally queue up the writing out of the resposne
  fetchQueue.push({
    fetchMethod: function(){console.log('calling do write page');doWritePage();},
    completedCheck: function(){return true;/*fire and forget*/},
    stepName: "Write Page"
  });

  //now kick off the fetches
  processFetchQueue();

  function getStatus(){
    fs.readFile('status.json', 'utf8', function (err,data) {
      if (err) {
        return console.log(err);
      }
      var dataJson = {};
      dataJson = JSON.parse(data);
      jobsData.name = dataJson.jobName;
      jobsData.lastSuccessfulFetch = dataJson.lastSuccessfullConnect;
      jobsData.lastAttemptedFetch = dataJson.lastConnectionAttempt;
      jobsData.connectionStatus = dataJson.connectionStatus;
    });
  };

  function getJobsData(type){
    var jobsDataIndex = indexOfJobType(type);
    console.log("type:" + type + ".json" + ":" + jobsDataIndex);

    fs.readFile(type + '.json', 'utf8', function (err,data) {
      if (err) {
        jobsData.jobsData[jobsDataIndex].completed = true;
        return console.log(err);
      }
      jobsData.jobsData[jobsDataIndex].data = JSON.parse(JSON.parse(JSON.stringify(data)));
      jobsData.jobsData[jobsDataIndex].completed = true;
    });
  };

  function fetchTotalsData(type){
    //now fetch the pass / fail numbers
    var jobsDataIndex = indexOfJobType(type);
    fs.readFile('cuke.json', 'utf8', function (err,data) {
      if (err) {
        jobsData.jobsData[jobsDataIndex].totalsCompleted = true;
        return console.log(err);
      }
      var cukeJson = JSON.parse(data);
      //spin through the cukePaths in cuke.json and pull out the path.job and total and totalFailing
      for (var cp = 0; cp < cukeJson.cukePaths.length; cp++) {
        var name = cukeJson.cukePaths[cp].path.job;
        var total = cukeJson.cukePaths[cp].total;
        var totalFailing = cukeJson.cukePaths[cp].totalFailing;
        var totalPassing = cukeJson.cukePaths[cp].totalPassing;
        var totalMissing = cukeJson.cukePaths[cp].totalMissing;
        //spin through the jobsData.jobsData[type].data[j].name = my name
        for (var t = 0; t < jobsData.jobsData.length; t++) {
          for (var j = 0; j < jobsData.jobsData[t].data.length; j++) {
            if (jobsData.jobsData[t].data[j].name == name) {
              //Queue ensures this always happens after job fetch, so can add fields to existing data[] item.
              jobsData.jobsData[t].data[j].total = total;
              jobsData.jobsData[t].data[j].totalFailing = totalFailing;
              jobsData.jobsData[t].data[j].totalPassing = totalPassing;
              jobsData.jobsData[t].data[j].totalMissing = totalMissing;
              break;
            }
          }
        }
      }
      jobsData.lastTotalsUpdate = cukeJson.lastUpdate;
      jobsData.jobsData[jobsDataIndex].totalsCompleted = true;
    });
  }
}

function writeTestJobsPage(response, orientation){
  response.write(writeHeader(orientation));

  function writeHeader(orientation){
    //TODO add styles to tidy up the HTML
    var result = '<!DOCTYPE html><html><head><meta charset="UTF-8"><title>Automated Test Results</title><style></style></head>\n';
    result += writeBody(orientation);
    result += '</html>';
    return result;
  }

  function writeBody(orientation){
    var result = '<body bgcolor="#181818" style="background-image: url(\'background-2.png\')">\n';
    result += writeTable(orientation);
    result += '</body>\n';
    return result;
  }

  function writeTable(orientation){
    var result = '<table align="center" style="color:#EAEAEA; text-align:center; font-family:\'Segoe UI\'">\n';
    result += writeRows(orientation);
    result += '</table>\n';
    return result;
  }

  function writeRows(orientation){
    var result = writeTitleRow(orientation);
    result += writeResultRows(orientation);
    return result;
  }

  function writeTitleRow(orientation){
    var result = '<tr style="font-size:72px">';
    result += '<td colspan="2" align="center" style="text-align:center">';
    result += jobsData.name;//'Automated Testing (demo)<br>Quality Owner View';
    result += '<br><div style="font-size:9px">';
    var d = new Date();
    result += d.toLocaleString() + '&nbsp;&nbsp;-&nbsp;&nbsp;';
    result += '(Jenkins connection: ' + jobsData.connectionStatus + ' @ ' + jobsData.lastSuccessfulFetch +
              ', last attempt: ' + jobsData.lastAttemptedFetch + '; Totals updated:' + jobsData.lastTotalsUpdate + ')';
    result += '</div>';
    result += '</td></tr>\n';
    return result;
  }

  function writeResultRows(orientation){
    var result = '';
    if (orientation == 'landscape'){
      //we have 1 row with two columns
      result += '<tr align="left" style="text-align:left">\n';
      result += '<td valign="top" style="vertical-align:top">' + writeResultsList('failed', orientation) + '</td>\n';
      result += '<td valign="top" style="vertical-align:top">' + writeResultsList('passed', orientation) + '</td>\n';
      result += '</tr>\n';
    } else {
      //we have 2 rows with one column
      result += '<tr align="left" style="text-align:left"><td valign="top" style="vertical-align:top">' +
        writeResultsList('failed', orientation) + '<td></tr>\n';
      result += '<tr align="left" style="text-align:left"><td valign="top" style="vertical-align:top">' +
        writeResultsList('passed', orientation) + '<td></tr>\n';
    }
    return result;
  }

  function writeResultsList(type, orientation){
    var jobsArray = jobsData.jobsData;
    var index = indexOfJobType(type);
    var jobKind = jobsArray[index].type;
    console.log(jobKind);
    var jobsList = jobsArray[index].data;
    var noOfJobs = jobsList.length;
    console.log('no of jobs = ' + noOfJobs);

    var fontSize = getFontSize(noOfJobs, orientation);
    var smallFontSize = 8;
    if (orientation == 'landscape') {
      smallFontSize = fontSize / 4;
    } else {
      smallFontSize = fontSize / 5;
    }
    var result = '\n<table border="0 cellspacing="0" cellpadding="0">';
    if (noOfJobs == 0) {
    } else {
      var radius = getCircleRadius(noOfJobs, orientation);
      for (var x = 0; x < jobsList.length; x++){
        //draw circle
        var circleFile = "";
        if ( (jobsList[x].color.indexOf('anime') > -1)
             //&& (orientation == 'landscape')
           ){
          circleFile = circleColour(type) + '-circle-anime.gif';
        } else {
          circleFile = circleColour(type) + '-circle.png';
        }
        result += '<tr><td width="' + radius + '" height="' + radius + '" background="' + circleFile + '" valign="middle" align="center"';
        result += ' style="background-size: ' + radius + 'px ' + radius + 'px; background-repeat:no-repeat; vertical-align:middle; text-align:center; line-spacing: 1; line-height:1.0">';
        //if (orientation == 'landscape') {
          if (jobsList[x].total) {
            result += (jobKind == 'passed') ? jobsList[x].totalPassing : '<u>' + jobsList[x].totalFailing + '</u><br>' + (jobsList[x].totalFailing + jobsList[x].totalPassing);
          }
        //}
        //draw job name
        result += '</td><td valign="middle" style="vertical-align:middle; font-size:' + fontSize + 'px; line-spacing: 1; line-height:1.1">';
        result += jobsList[x].name;
        if (jobKind == 'failed') {
          result += '<br><div style="font-size:' + smallFontSize + 'px">';
          result += 'Last successful build: ' + (jobsList[x].lastSuccessfulBuild ? jobsList[x].lastSuccessfulBuild.id : 'None!') + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
          result += claimedBy(jobsList[x]);
          result += '</div>';
        }
        result += '</td></tr>\n';
      }
    }
    result += '</table>\n';
    return result;
  }

  function claimedBy(job) {
    result = '';
    if ((job) && (job.lastUnsuccessfulBuild) && (job.lastUnsuccessfulBuild.actions)) {
      var claimedByList = job.lastUnsuccessfulBuild.actions.filter(function(action) {return (action.hasOwnProperty('claimedBy'))})
      var claimedBy = null;
      if (claimedByList.length > 0) {
        claimedBy = claimedByList[0].claimedBy;
      }
      var result = 'Claimed by: ';
      if (claimedBy == null) {
        result += 'Not claimed yet!';
      } else {
        result += claimedBy;
      }
    }
    return result;
  }

  function getCircleRadius(count, orientation){
    if (orientation == 'landscape') {
      switch(scaling(count)) {
        case 'large': return 60;
        case 'medium': return 40;
        case 'small': return 30;
        //assume very small
        default: return 24;
      }
    } else {
      return 60;
    }
  }

  function getFontSize(count, orientation){
    if (orientation == 'landscape') {
      switch(scaling(count)) {
        case 'large': return 48;
        case 'medium': return 32;
        case 'small': return 24;
        //assume very small
        default: return 20;
      }
    } else {
      return 52;
    }
  }

  function scaling(count){
    if (count <= 8) {
      return 'large'
    } else if (count <= 12) {
      return 'medium'
    } else if (count <= 16) {
      return 'small'
    } else {
      return 'very-small'
    }
  }

  function circleColour(type) {
    if (type == 'passed') {
      return 'green'
    } else {
      return 'red'
    }
  }
}
