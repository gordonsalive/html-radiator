var http = require('http');
var fs = require('fs');

var user = "chapmand";
var token = "7b94c59e8f4ea17721122a1f6b0de568";
var userAndToken = user + ':' + token;
var outputFilename = 'my.json';
var configObj = require("./config.json");
console.log('================================================================================');
console.log('Start');
console.log('================================================================================');
console.log(configObj);

var options = {
  hostname: configObj.hostname,
  port: 80,
  path: configObj.path,
  method: 'POST',
  auth: userAndToken
};

var allOfBody = '';
var status = { jobName: '',
               connectionStatus: 'not connected',
               lastSuccessfullConnect: 'N/A',
               lastConnectionAttempt: 'N/A'};

(function doMainStuff() {
  allOfBody = '';
  writeOutStatus();

  var req = http.request(options, function(res) {
    console.log('STATUS: ' + res.statusCode);
    //console.log('HEADERS: ' + JSON.stringify(res.headers));
    res.setEncoding('utf8');
    res.on('data', function (chunk) {
      allOfBody = allOfBody + chunk;
    });
    res.on('end', function() {
      //console.log('No more data in response.');
      //console.log(allOfBody);
      writeOutBody();
      var d = new Date();
      console.log('mainLoop:' + d.toTimeString());
      setTimeout(doMainStuff, 30 * 1000);
    })
  });
  req.on('error', function(e) {
    console.log('problem with request: ' + e.message);
    status.connectionStatus = 'not connected';
    setTimeout(doMainStuff, 300 * 1000);//try again in 5 minutes
  });
  // write data to request body
  req.write(' ');
  //req.write(postData);
  req.end();
})();



function writeOutBody(){
  var d = new Date();
  status.lastSuccessfullConnect = d.toLocaleString();
  status.connectionStatus = 'connected';

  var allOfBodyJson = JSON.parse(allOfBody);
  var passedJson = allOfBodyJson.jobs.filter(function (job) {
    return (job.color.lastIndexOf('blue') === 0);//starts with blue
  });
  var failedJson = allOfBodyJson.jobs.filter(function (job) {
    return (job.color.lastIndexOf('blue') != 0);//starts with read or anything else not blue
  });

  fs.writeFile('passed.json', JSON.stringify(passedJson), function(err) {
    if(err) {
      console.log(err);
    } else {
      //console.log("JSON saved to " + 'passed.json');
    }
  });
  fs.writeFile('failed.json', JSON.stringify(failedJson), function(err) {
    if(err) {
      console.log(err);
    } else {
      //console.log("JSON saved to " + 'failed.json');
    }
  });

  writeOutStatus();
}

function writeOutStatus(){
  //write out the data to status.json
  status.jobName = configObj.name;
  var d = new Date();
  status.lastConnectionAttempt = d.toLocaleString();
  fs.writeFile('status.json', JSON.stringify(status), function(err) {
    if(err) {
      console.log(err);
    } else {
      //console.log("JSON saved to " + 'status.json');
    }
  });
}
