Usage
=====

Start up the fetch-json script to fetch passed and failed jobs from Jenkins every 10 seconds
Then, start up the radiator script, which is a http server and will parse json files and create page showing passing and failing jobs.

To Do
=====
> Make more of it configurable, e.g. port, refresh time...
* This is version 1, next version will use a framework to massively tidy up code

Additional
==========
Use nodemon in development to automatically restart the scripts if any changes
Use forever to run the server and the json-fetch scripts continuously
