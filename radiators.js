console.log('================================================================================');
console.log('Start');
console.log('================================================================================');

var http = require('http');
var url = require('url');
var qs = require('querystring');
var fs = require('fs');
var Qfs = require("q-io/fs");
var Q = require("q");
Q.longStackSupport = true;
var _ = require("underscore");

var view = 'Quality Owner View';
var xmas = false;

http.createServer(function (request, response) {
    var requestUrl = url.parse(request.url);
    var action = requestUrl.pathname;
    console.log(request.method + ':' + action);
    if (_.contains(['/radiators.html', '/radiators-vertical.html'], action)) {
        var orientation = (action == '/radiators.html') ? 'landscape' : 'portrait';

        view = 'Quality Owner View';
        xmas = false;
        var params;
        if (request.method == 'POST') {
            var body = '';
            request.on('data', function (data) {
                body += data;
            });
            request.on('end', function () {
                params = qs.parse(body);
            });
        } else if (request.method == 'GET') {
            params = url.parse(request.url, true).query;
        }
        if (params.view) {
            view = params.view;
        }
        console.log('view = ' + view);
        if (params.xmas) {
            xmas = true;
        }

        response.writeHead(200, {
            "Content-Type": "text/html"
        });
        fetchData()
            .then(
                function () {
                    return Q.Promise(function (resolve, reject, notify) {
                        try {
                            try {
                                //console.log(JSON.stringify(jobsData));
                                writeTestJobsPage(response, orientation);
                            } finally {
                                response.end()
                            }
                            resolve();
                        } catch (err) {
                            reject(err)
                        }
                    });
                })
            .fail(console.error);
    } else
    if (_.contains(
    ['/brendan.jpeg'], action)) {
        var img = fs.readFileSync('.' + action);
        response.writeHead(200, {
            'Content-Type': 'image/jpeg'
        });
        response.end(img, 'binary');
    } else
    if (_.contains(
    [
      '/red-circle.png',
      '/green-circle.png',
      '/red-xmas.png',
      '/green-xmas.png',
      '/background-2.png'], action)) {
        var img = fs.readFileSync('.' + action);
        response.writeHead(200, {
            'Content-Type': 'image/png'
        });
        response.end(img, 'binary');
    } else
    if (_.contains(
     [
       '/red-circle-anime.gif',
       '/green-circle-anime.gif',
       '/red-xmas-anime.gif',
       '/green-xmas-anime.gif'
      ], action)) {
        var img = fs.readFileSync('.' + action);
        response.writeHead(200, {
            'Content-Type': 'image/gif'
        });
        response.end(img, 'binary');
    } else {
        console.log('--FAILED:' + action);
        response.writeHead(500);
        response.end()
    }
}).listen(8083);

var jobsData = {
    name: 'Automated Test Results',
    lastSuccessfulFetch: '',
    lastAttemptedFetch: '',
    connectionStatus: 'not connected',
    jobsData: [{
            type: 'passed',
            data: {
                someView: []
            }
        },
        {
            type: 'failed',
            data: {
                someView: []
            }
        }]
}

function indexOfJobType(type) {
    return jobsData.jobsData.map(function (job) {
        return job.type;
    }).indexOf(type);
}

function fetchData() {
    console.log('fetch data');

    var statusPromise = getStatus();
    return statusPromise
        .then(function () {
            return Q.all([
      getJobsData('passed').then(function () {
                    return fetchTotalsData('passed')
                }),
      getJobsData('failed').then(function () {
                    return fetchTotalsData('failed')
                })
    ])
        });

    function getStatus() {
        console.log('getStatus');
        return Qfs.read("status.json")
            .then(function (data) {
                return Q.Promise(function (resolve, reject, notify) {
                    try {
                        var dataJson = {};
                        dataJson = JSON.parse(data);
                        jobsData.name = dataJson.jobName;
                        jobsData.lastSuccessfulFetch = dataJson.lastSuccessfullConnect;
                        jobsData.lastAttemptedFetch = dataJson.lastConnectionAttempt;
                        jobsData.connectionStatus = dataJson.connectionStatus;
                        console.log('updated status');
                        resolve(dataJson);
                    } catch (err) {
                        reject(err)
                    }
                });
            }) //then
    };

    function getJobsData(type) {
        console.log('getJobsData:' + type);
        var jobsDataIndex = indexOfJobType(type);
        //console.log("type:" + type + ".json" + ":" + jobsDataIndex);

        return Qfs.read(type + '.json')
            .then(function (data) {
                return Q.Promise(function (resolve, reject, notify) {
                    try {
                        //console.log('data:');
                        //console.log(JSON.stringify(data));
                        jobsData.jobsData[jobsDataIndex].data = JSON.parse(JSON.parse(JSON.stringify(data)));
                        //console.log(jobsData.jobsData[jobsDataIndex].data);
                        console.log('updated jobsData:' + type);
                        resolve();
                    } catch (err) {
                        reject(err)
                    }
                });
            }); //then
    };

    function fetchTotalsData(type) {
        console.log('fetchTotalsData:' + type);
        //now fetch the pass / fail numbers
        var jobsDataIndex = indexOfJobType(type);
        return Qfs.read("cuke.json")
            .then(function (data) {
                return Q.Promise(function (resolve, reject, notify) {
                    try {
                        var cukeJson = JSON.parse(data);
                        //spin through the types of jobs (passing or failing)
                        for (var t = 0; t < jobsData.jobsData.length; t++) {
                            //spin through the job and look up into the cuke data by job name
                            for (var j = 0; j < jobsData.jobsData[t].data[view].length; j++) {
                                var cuke = cukeJson.cukes[jobsData.jobsData[t].data[view][j].name];
                                if (cuke) {
                                    jobsData.jobsData[t].data[view][j].total = cuke.total;
                                    jobsData.jobsData[t].data[view][j].totalFailing = cuke.totalFailing;
                                    jobsData.jobsData[t].data[view][j].totalPassing = cuke.totalPassing;
                                    jobsData.jobsData[t].data[view][j].totalMissing = cuke.totalMissing;
                                }
                            }
                        }

                        jobsData.lastTotalsUpdate = cukeJson.lastUpdate;
                        console.log('updated Totals:' + type);
                        resolve();
                    } catch (err) {
                        reject(err)
                    }
                });
            }); //then
    }
}

function writeTestJobsPage(response, orientation) {
    response.write(writeHeader(orientation));

    function writeHeader(orientation) {
        //TODO add styles to tidy up the HTML
        var result = '<!DOCTYPE html><html><head><meta charset="UTF-8"><title>Automated Test Results</title><style></style></head>\n';
        result += writeBody(orientation);
        result += '</html>';
        return result;
    }

    function writeBody(orientation) {
        var result = '<body bgcolor="#181818" style="background-image: url(\'background-2.png\')">\n';
        result += writeTable(orientation);
        result += '</body>\n';
        return result;
    }

    function writeTable(orientation) {
        var result = '<table align="center" style="color:#EAEAEA; text-align:center; font-family:\'Segoe UI\'">\n';
        result += writeRows(orientation);
        result += '</table>\n';
        return result;
    }

    function writeRows(orientation) {
        var result = writeTitleRow(orientation);
        result += writeResultRows(orientation);
        return result;
    }

    function writeTitleRow(orientation) {
        var result = '<tr style="font-size:72px">';
        result += '<td colspan="2" align="center" style="text-align:center">';
        result += view; //jobsData.name;//'Automated Testing (demo)<br>Quality Owner View';
        result += '<br><div style="font-size:9px">';
        var d = new Date();
        result += d.toLocaleString() + '&nbsp;&nbsp;-&nbsp;&nbsp;';
        result += '(View: ' + jobsData.connectionStatus + ' @ ' + jobsData.lastSuccessfulFetch +
            ', last attempt: ' + jobsData.lastAttemptedFetch + '; Totals:' + jobsData.lastTotalsUpdate + ')';
        result += '</div>';
        result += '</td></tr>\n';
        return result;
    }

    function writeResultRows(orientation) {
        var result = '';
        if (orientation == 'landscape') {
            //we have 1 row with two columns
            result += '<tr align="left" style="text-align:left">\n';
            result += '<td valign="top" style="vertical-align:top">' + writeResultsList('failed', orientation) + '</td>\n';
            result += '<td valign="top" style="vertical-align:top">' + writeResultsList('passed', orientation) + '</td>\n';
            result += '</tr>\n';
        } else {
            //we have 2 rows with one column
            result += '<tr align="left" style="text-align:left"><td valign="top" style="vertical-align:top">' +
                writeResultsList('failed', orientation) + '<td></tr>\n';
            result += '<tr align="left" style="text-align:left"><td valign="top" style="vertical-align:top">' +
                writeResultsList('passed', orientation) + '<td></tr>\n';
        }
        return result;
    }

    function writeResultsList(type, orientation) {
        var jobsArray = jobsData.jobsData;
        var index = indexOfJobType(type);
        var jobKind = jobsArray[index].type;
        console.log('writeResultsList:' + jobKind);
        var jobsList = jobsArray[index].data[view];
        var noOfJobs = jobsList.length;
        console.log('no of jobs = ' + noOfJobs);

        var fontSize = getFontSize(noOfJobs, orientation);
        var smallFontSize = 8;
        if (orientation == 'landscape') {
            smallFontSize = fontSize / 4;
        } else {
            smallFontSize = fontSize / 5;
        }
        var result = '\n<table border="0 cellspacing="0" cellpadding="0">';
        if (noOfJobs == 0) {} else {
            var radius = getCircleRadius(noOfJobs, orientation);
            for (var x = 0; x < jobsList.length; x++) {
                //draw circle
                var circleFile = "";
                if (xmas) {
                    circleFile = circleColour(type) + '-xmas'
                } else {
                    circleFile = circleColour(type) + '-circle'
                }

                if ((jobsList[x].color.indexOf('anime') > -1)) {
                    circleFile = circleFile + '-anime.gif';
                } else {
                    circleFile = circleFile + '.png';
                }
                result += '<tr><td width="' + radius + '" height="' + radius + '" background="' + circleFile + '" valign="middle" align="center"';
                if ((xmas) && (type == 'failed')) {
                    result += ' style="color:Teal; font-weight:bold; background-size: ' + radius + 'px ' + radius + 'px; background-repeat:no-repeat; vertical-align:middle; text-align:center; line-spacing: 1; line-height:1.0">';
                } else {
                    result += ' style="font-weight:bold; background-size: ' + radius + 'px ' + radius + 'px; background-repeat:no-repeat; vertical-align:middle; text-align:center; line-spacing: 1; line-height:1.0">';
                }
                //if (orientation == 'landscape') {
                if (jobsList[x].total) {
                    result += (jobKind == 'passed') ? jobsList[x].totalPassing : '<u>' + jobsList[x].totalFailing + '</u><br>' + (jobsList[x].totalFailing + jobsList[x].totalPassing);
                }
                //}
                //draw job name
                result += '</td><td valign="middle" style="vertical-align:middle; font-size:' + fontSize + 'px; line-spacing: 1; line-height:1.1">';
                result += jobsList[x].name;
                if ((jobKind == 'failed') && (orientation == 'landscape')) {
                    result += '<br><div style="font-size:' + smallFontSize + 'px">';
                    result += 'Last successful build: ' + (jobsList[x].lastSuccessfulBuild ? jobsList[x].lastSuccessfulBuild.id : 'None!') + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    result += claimedBy(jobsList[x]);
                    result += '</div>';
                }
                result += '</td></tr>\n';
            }
        }
        result += '</table>\n';
        return result;
    }

    function claimedBy(job) {
        result = '';
        if ((job) && (job.lastUnsuccessfulBuild) && (job.lastUnsuccessfulBuild.actions)) {
            var claimedByList = job.lastUnsuccessfulBuild.actions.filter(function (action) {
                return (action.hasOwnProperty('claimedBy'))
            })
            var claimedBy = null;
            if (claimedByList.length > 0) {
                claimedBy = claimedByList[0].claimedBy;
            }
            var result = 'Claimed by: ';
            if (claimedBy == null) {
                result += 'Not claimed yet!';
            } else {
                result += claimedBy;
            }
        }
        return result;
    }

    function getCircleRadius(count, orientation) {
        if (orientation == 'landscape') {
            switch (scaling(count)) {
            case 'large':
                return 60;
            case 'medium':
                return 40;
            case 'small':
                return 30;
                //assume very small
            default:
                return 24;
            }
        } else {
            return 40;
        }
    }

    function getFontSize(count, orientation) {
        if (orientation == 'landscape') {
            switch (scaling(count)) {
            case 'large':
                return 48;
            case 'medium':
                return 32;
            case 'small':
                return 24;
                //assume very small
            default:
                return 20;
            }
        } else {
            return 32;
        }
    }

    function scaling(count) {
        if (count <= 8) {
            return 'large'
        } else if (count <= 12) {
            return 'medium'
        } else if (count <= 16) {
            return 'small'
        } else {
            return 'very-small'
        }
    }

    function circleColour(type) {
        if (type == 'passed') {
            return 'green'
        } else {
            return 'red'
        }
    }
}